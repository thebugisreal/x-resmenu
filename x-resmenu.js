﻿// menu toggle
$(function () {
	var html = $('html, body'),
			navContainer = $('#x-resMenu > nav'),
			navToggle = $('#x-resMenu > .toggle'),
			navDropdownToggle = $('#x-resMenu .dropdown');
			overlay = $("<div class='overlay'></div> ");

	//Nav toggle
	navToggle.on('click', function (e) {
		overlay.toggle();
		var $this = $(this);
		e.preventDefault();
		$this.toggleClass('show');
		navContainer.toggleClass('visible');
		html.toggleClass('nav-open');
	});

	$("body").prepend(overlay);
	overlay.click(function () {
		navToggle.trigger('click');
	});

	// Nav dropdown toggle
	navDropdownToggle.on('click', function () {
		var $this = $(this);
		$this.children().children().removeClass('show');
		$this.toggleClass('show').siblings().removeClass('show');

		if ($this.children('ul').hasClass('open-nav')) {
			$this.children('ul').removeClass('open-nav');
			$this.children('ul').slideUp(350);
		}
		else {
			$this.parent().parent().find('li ul').removeClass('open-nav');
			$this.parent().parent().find('li ul').slideUp(350);
			$this.children('ul').toggleClass('open-nav');
			$this.children('ul').slideToggle(350);
		}
	});

	// Prevent click events from firing on children of navDropdownToggle
	navDropdownToggle.on('click', '*', function (e) {
		e.stopPropagation();
	});
});