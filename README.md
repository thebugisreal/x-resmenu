﻿## Cài đặt

Yêu cầu: **jQuery, FontAwesome**.

Clone về và nhúng hai file sau vào website của bạn.
```markdown
<link href="x-resmenu.min.css" rel="stylesheet" />
```

```markdown
<script src="x-resmenu.min.js"></script>
```

## Sử dụng

1. Tạo một thẻ với ID là `x-resMenu`

```markdown
<section id="x-resMenu"></section>
```

2. Chèn vào trong thanh navbar và button theo cấu trúc mặc định sau:

```markdown
<nav>
 <ul>
  <li><a href="#">content 1</a></li>
  <li><a href="#">content 2</a></li>
  <li><a href="#">content 3</a></li>
 </ul>
</nav>
<button class="toggle"><span></span></button>
```

3. Toàn bộ ví dụ

```markdown
<section id="x-resMenu">
 <nav>
  <ul>
   <li><a href="#">content 1</a></li>
   <li><a href="#">content 2</a></li>
   <li><a href="#">content 3</a></li>
  </ul>
 </nav>
 <button class="toggle"><span></span></button>
</section>
```

## Nâng cao

Sử dụng class `.active` trên thẻ `li` nếu muốn highlight thẻ đang được chọn.

```markdown
<li class="active"><a href="#">content 1</a></li>
```

Sử dụng class `.dropdown` trên thẻ `li` nào chứa menu con bên trong.

```markdown
<li class="dropdown">
 <a href="#">content 1</a>
 <ul>
  <li><a href="#">child content 1</a></li>
 </ul>
</li>
```